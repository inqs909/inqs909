---
bio: My research focuses on developing survival models to better understand trends.
education:
  courses:
  - course: PhD in Applied Statistics
    institution: University of California, Riverside
  - course: MPH in Biometry
    institution: San Diego State University
    year: 2015
  - course: BS in Biology
    institution: California State University, Monterey Bay
    year: 2013
email: ""
highlight_name: false
interests:
- Longitudinal Models
- Survival Models
- Health Equity
- Education
- Algorithmic Bias
organizations:
- name: California State University Channel Islands
  url: https://www.csuci.edu/
role: Assistant Professor of Statistics
social:
- icon: github
  icon_pack: fab
  link: https://github.com/inqs909
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/inqs909
superuser: true
title: Isaac Quintanilla Salinas
---

Isaac Quintanilla Salinas is a recent graduate from the [Department of Statistics](https://statistics.ucr.edu/) at the [University of California, Riverside](https://www.ucr.edu) with a PhD in Applied Statistics. He is joining the [Department of Mathematics](https://math.csuci.edu/) at the California State University Channel Islands this upcoming Fall 2022. Prior to his doctoral research, Dr. Quintanilla Salinas has earned his MPH from San Diego State University and BS in Biology from the California State University Monterey Bay.

Dr. Quintanilla Salinas research focuses on developing statistical models to understand the association between survival and longitudinal outcomes. His dissertation "Multilevel Time-Varying Joint Models for Longitudinal and Survival Outcomes" develops a novel model to address challenges of modeling dependent outcomes.

Pronouns: [he/him/his/él](https://out.ucr.edu/pronouns-matter)

{{< icon name="download" pack="fas" >}} Download my {{< staticref "uploads/CV.pdf" "newtab" >}}CV{{< /staticref >}}.



